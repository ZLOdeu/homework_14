﻿#include <iostream>
#include <string>
#include <stdint.h>
#include <iomanip>

int main()
{
    using namespace std::literals;

    std::string str1 = "hello";

    // Creating a string using string literal
    auto str2 = "world"s;

    // Concatenating strings
    std::string str3 = str1 + " " + str2;

    // Print out the result
    std::cout << str3 << '\n';
    std::cout << str1[0] << '\n';
    std::cout << str1 << '\n';
    std::cout << str2[4] << '\n';
    std::cout << str2 << '\n';
    std::cout << str3.length() << "\n";

    return 0;

}